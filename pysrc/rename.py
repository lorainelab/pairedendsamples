#!/usr/bin/env python

"""Rename reads to make visual analysis more convenient."""

import fileinput, sys

def main():
    d={}
    oldnames=[]
    for line in fileinput.input():
        if line.startswith('@'):
            sys.stdout.write(line)
        else:
            toks=line.rstrip().split('\t')
            name=toks[0]
            if d.has_key(name):
                d[name].append(toks)
            else:
                d[name]=[toks]
                oldnames.append(name)
    i = 1
    for name in oldnames:
        tokss=d[name]
        if len(tokss) > 1:
            newname='M%i-%i'%(len(tokss),i)
        else:
            newname='S-%i'%i
        for toks in tokss:
            toks[0]=newname
            sys.stdout.write('\t'.join(toks)+'\n')
        i=i+1
    # and that's it!

if __name__ == '__main__':
    main()
