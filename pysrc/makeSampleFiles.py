#!/usr/bin/env python

import pysam,os,sys,argparse,re

ex=\
"""
Making sample files with read alignments matching requirements in https://jira.transvar.org/browse/IGBF-107
Using accepted_hits.bam from Harper Lab paired end alignments CW1 sample.

Files:
* R1 maps but R2 doesn't: R1MapsR2NotMaps.sam
* R2 maps but R2 doesn't: R2MapsR1NotMaps.sam
* R1 and R2 each map multiple times to the same sequence: R1orR2MultiMap-1.sam, -2.sam, etc
"""

# SR45A region
# chr1:2,257,056-2,260,371 
# U2AF65A region
# chr4:17,292,922-17,298,846
r1='chr1:2,257,056-2,260,371'
bam1='accepted_hits.bam'

def main(bam=bam1):
    infile=pysam.Samfile(bam,"rb")
    (seq,start,end)=getRegion(r1)
    lst=getRecords(samfile=infile,seq=seq,start=start,end=end)

    fname='R1MapsR2NoMap.sam'
    reads_to_write=getR1MapsR2Not(lst)
    write_reads(fname,infile,reads_to_write)

    fname='R2MapsR1NoMap.sam'
    reads_to_write=getR2MapsR1Not(lst)
    write_reads(fname,infile,reads_to_write)
    
    fname_base='R1orR2MultiMap'
    reads_to_write=getR1orR2MultiMap(lst)
    d={}
    for read in reads_to_write:
        qname=read.qname
        if not d.has_key(qname):
            d[qname]=[]
        d[qname].append(read)
    i=0
    for qname in d.keys():
        i=i+1
        reads_to_write=d[qname]
        fname=fname_base+'-'+str(i)+'.sam'
        write_reads(fname,infile,reads_to_write)
    return lst

def write_reads(fname,template,reads_to_write):
    fh=pysam.Samfile(fname,'wh',template=template)
    for read in reads_to_write: fh.write(read)
    fh.close()    

# R1 maps but R2 doesn't
def getR1MapsR2Not(lst):
    return(filter(lambda x:x.is_read1 and x.is_paired and not x.is_unmapped and x.mate_is_unmapped,lst))

# R2 maps but R1 doesn't
def getR2MapsR1Not(lst):
    return(filter(lambda x:x.is_read2 and x.is_paired and not x.is_unmapped and x.mate_is_unmapped,lst))

# R1, R2 or both map more than once
def getR1orR2MultiMap(lst):
    return(filter(lambda x: not x.mate_is_unmapped and filter(lambda x:x[0]=='NH',x.tags)[0][1]>1,lst))

def getRecords(samfile=None,seq=None,start=None,end=None):
    iter=samfile.fetch(seq,start,end)
    lst=[]
    for record in iter:
        lst.append(record)
    return lst

def getRegion(s):
    seqname,coords=s.split(':')
    coords=re.sub(',','',coords)
    coords=coords.split('-')
    coords=map(lambda x:int(x),coords)
    return (seqname,coords[0],coords[1])

if __name__ == '__main__':
    parser=argparse.ArgumentParser(description=ex)
    #parser.add_argument('-b',action="store",default=None,dest="bam",
    #                    help="BAM file with index, eg., Control.bam [required]")
    #parser.add_argument('-r',action="store",default='chr1:2,257,056-2,260,371',dest="region",
    #help="region in IGB format [required, commas optional]")
    main()
    #args=parser.parse_args()
    #main(bam=args.bam,region=args.region)
