#!/usr/bin/env python

import HTSeq, pysam,os,sys,argparse,re

ex=\
"""
Making sample files with read alignments matching requirements in https://jira.transvar.org/browse/IGBF-107
Using accepted_hits.bam from Harper Lab paired end alignments CW1 sample.

This script creates files containing alignments for read pairs where each member of the
pair aligns exactly once and where the reads either overlap or they don't.

Note that two members of a pair can overlap if the original fragment that was sequenced
was shorter than the combined read length. For example, if the fragment was only 150
bases and the read length was 100 bases, then the last 50 bases of R1 and the final 50
bases of R2 will be reverse complements.

Files:
* R1 and R2 align exactly once to the same sequence but don't overlap: R1R2MapOnceNoOverlap.sam
* R1 and R2 align exactly once to the same sequence but overlap: R1R2MapOnceOverlap.sam

To make testing easier, reads are named 1, 2, 3, etc. Mates from the same fragment have the
same name.

This was useful:

http://www-huber.embl.de/users/anders/HTSeq/doc/tour.html#genomic-intervals-and-genomic-arrays

Note that because I'm not entirely confident that I understand the SAM spec, I'm going to
collect reads that match these criteria from an alignment file and modify them 
as needed.
"""

# SR45A region
# chr1:2,257,056-2,260,371 
# U2AF65A region
# chr4:17,292,922-17,298,846
r1='chr1:2,257,056-2,260,371'
bam1='accepted_hits.bam'

# in interpreter, just run main
# defaults are bam file is accepted_hits.bam
# region is SR45A in Arabidopsis thaliana June 2009 genome
def main(bam=bam1,region=r1):
    reader=HTSeq.BAM_Reader(bam)
    lst=getRecords(reader,region)
    d=getR1R2MapOnceNoOverlap(lst)
    lst2=renameAndMakeList(d)
    fname='R1R2MapOnceNoOverlap.sam'
    write_reads(fname,bam,lst2)
    fname='R1R2MapOnceOverlap.sam'
    d=getR1R2MapOnceOverlap(lst)
    lst2=renameAndMakeList(d)
    write_reads(fname,bam,lst2)
    return lst # useful in interactive mode

# rename the reads for easier viewing,
# return them as a list
def renameAndMakeList(d):
    lst2=[]
    i=1
    for name in d.keys():
        for aln in d[name]:
            aln.read.name=str(i)
            lst2.append(aln)
        i = i + 1
    return lst2

# write the reads to plain text SAM file with header
def write_reads(fname,bam,reads_to_write):
    infile=pysam.Samfile(bam,"rb") # to get header
    fh=pysam.Samfile(fname,'wh',template=infile) # writer
    fh.close() # surely there is a better way
    fh=open(fname,'a')
    for read in reads_to_write: fh.write(read.get_sam_line()+'\n') 
    fh.close()    

# use NH tag to get alignments for single-mapping reads
def getSingleMappers(lst):
    return filter(lambda x: 'NH:i:1' in x.raw_optional_fields(),lst)

# build dictionary with read names as keys, values are alignments
def getSameName(lst):
    d = {}
    for aln in lst:
        name=aln.read.name
        if not d.has_key(name):
            d[name]=[]
        d[name].append(aln)
    return d

def getMateMapped(lst):
    return filter(lambda x:x.mate_aligned,lst)

# maybe mapped outside? HWI-ST0860:302:H0PW1ADXX:2:2216:17281:20108
# get like this:
# read=filter(lambda x:x.read.name=='HWI-ST0860:302:H0PW1ADXX:2:2216:17281:20108',lst)
#>>> read.paired_end
#True
#>>> read.mate_aligned
#True
#>>> read.mate_start
#<GenomicPosition object 'chr1':611346, strand '+'>
#>>> read.iv
#<GenomicInterval object 'chr1', [2259509,2259559), strand '+'>

def getSingleMappingPairs(lst):
    # get reads that mapped exactly once
    lst2 = getSingleMappers(lst)
    lst2 = getMateMapped(lst2)
    # put pairs together
    d = getSameName(lst2)
    # note: some mates might not be in the list, see above commments
    return d

# R1 and R2 map exactly once and do not overlap
def getR1R2MapOnceNoOverlap(lst):
    d=getSingleMappingPairs(lst)
    to_return={}
    for name in d.keys():
        # sys.stderr.write('testing: %s\n'%name)
        pair=d[name]
        if not len(pair)==2:
            sys.stderr.write("Alert: Pair %s has %i alignments. Skipping it.\n"%(name,len(pair)))
            continue
        a=pair[0]
        b=pair[1]
        if not overlaps(a.iv,b.iv):
            to_return[name]=[a,b]
    return to_return

# R1 and R2 map exactly once and overlap
def getR1R2MapOnceOverlap(lst):
    d=getSingleMappingPairs(lst)
    to_return={}
    for name in d.keys():
        # sys.stderr.write('testing: %s\n'%name)
        pair=d[name]
        if not len(pair)==2:
            sys.stderr.write("Alert: Pair %s has %i alignments. Skipping it.\n"%(name,len(pair)))
            continue
        a=pair[0]
        b=pair[1]
        if overlaps(a.iv,b.iv):
            to_return[name]=[a,b]
    return to_return

# a and b are GenomicInterval objects
# see http://www-huber.embl.de/users/anders/HTSeq/doc/genomic.html
def overlaps(a,b):
    if a.chrom==b.chrom:
        if a.start>a.end or b.start>b.end:
            raise ValueError("Start for GenomicInterval larger than end.")
        # the leftmost end and the rightmost start define the region of overlap
        # if that region has non-zero length, then the two regions overlap
        return min(a.end,b.end)-max(a.start,b.start)>0

# tried this first - see below
# But can't use it because overlaps returns false if intervals are on different strands
# http://www-huber.embl.de/users/anders/HTSeq/doc/genomic.html
#        if not a.iv.overlaps(b.iv): 
#            to_return[name]=[a,b]



def getRecords(reader,region):
    region=re.sub(',','',region)
    lst=[]
    for read in reader.fetch(region=region):
        lst.append(read)
    return lst

if __name__ == '__main__':
    parser=argparse.ArgumentParser(description=ex)
    #parser.add_argument('-b',action="store",default=None,dest="bam",
    #                    help="BAM file with index, eg., Control.bam [required]")
    #parser.add_argument('-r',action="store",default='chr1:2,257,056-2,260,371',dest="region",
    #help="region in IGB format [required, commas optional]")
    main()
    #args=parser.parse_args()
    #main(bam=args.bam,region=args.region)
