# Introduction

This repository contains test data files, documentation, and design
notes created to enable adding a new paired-end read alignment
visualization to Integrated Genome Browser. Our goal is to add new
ways to visualize paired end read alignments in IGB, building on work that
has been done in other genome browsers. Wherever possible, we'll
use but also improve upon visualization techniques implemented in
other browsers.

Major types of data we would like to visualize include

* Paired end RNA-Seq read alignments 
* Paired end read alignments from genome re-sequencing projects

For now, we assume that alignments will be stored using the SAM
format, described here: http://samtools.sourceforge.net/samtools.shtml.

# About BAM and SAM formats

BAM is a binary file format for representing alignments of sequences onto
a larger reference sequence, such as a genome assembly. SAM is the human-
readable, ASCII variant of BAM. Both are tab-separated formats. Each line
of data represents a single alignment.

The SAM and BAM formats contain identicial data except that BAM
uses 0-based coordinates, also called interbase. SAM is 1-based. This matters
when making API calls to BAM-format readers as they will return start and
end positions in interbase, not 1-based coordinates. 

For more infromation about interbase versus one-based coordiante
systems, see: http://gmod.org/wiki/Introduction_to_Chado#Interbase_Coordinates

## Data directories

This repo contains directories with SAM and BAM files. Names of the 
files mostly explain what the files contain and how they were generated. Ask Ann is something
isn't clear.

### Files in the Arabidopsis directory

Made by makeSamplesFiles.py using data from an Arabidopsis paired-end
RNA-Seq experiment:

* R1 maps but R2 doesn't: R1MapsR2NotMaps.sam
* R2 maps but R2 doesn't: R2MapsR1NotMaps.sam
* R1 and R2 each map multiple times to the same sequence: R1orR2MultiMap-1.sam, -2.sam, etc

Made by running samtools view and renaming reads:

* position_sorted.bam

## Introduction to paired end reads

Illumina sequencing instruments can produce both single-end and paired-end
reads. Single-end reads don't have a mate, but paired ends reads do
have mates. The first "end" of a pair  is called read 1 and the
second "end" of a pair is called read 2. They come from different
strands and so when aligned to a gneome, they should map to different
strands of the reference sequence. 

The Picard library (http://picard.sourceforge.net/) has methods
and objects for accessing information about reads, such as whether a read
is the first or second read of a pair. See: net.sf.samtools.SAMRecord, which
models a single read alignment in a SAM/BAM file.

### Simple explanation for Illumina paired end and single end sequencing

Imagine that you have an enormous bag of double-stranded strings that
range in size between 300 and 500 letters (bases) in length.

By double-stranded, I mean that each string has a forward and reverse
strand:

Ex)

```
Forward strand: A G T C T C C G G
Reverse strand: T C A G A G G C C 
```

You don't know what the sequence of letters is for each of these millions of
double-stranded strings. You use a sequencer for that.

Running a sequencer is complicated, but for the purposes of
understanding the data, imagine that all you have to do is dump your
millions of strings into the machine and turn it on.

In the first step of the sequencing, the sequencer grabs hold of each of the millions of
double-strand strings you dumped inside it and glues one end of each string
onto a flat surface called a "flow cell."

It glues the strings onto the flat surface in mostly random order, but each string
is nicely spaced a certain distance from its neighbors so that the machine can
tell them apart. 

Next, the sequencing phase begins. The machine uses chemistry (technically called
sequencing by synthesis) to start determining the sequence of the strings starting
from one end of each string. Base by base, the sequencer reads the 
sequence of each one of the millions of strings, starting at the free end 
and proceeding toward the end that is stuck to the plate.

The sequencer is flexible in that before you start the reading the sequences,
you can tell it how many bases you want it read from the end
of each string, up to 150 for the most modern Illumina
instruments. However, the reads you get from a single "run" of the
instrument will always be the same length. So when I say that I did
"50 cycle sequencing" I mean that I asked for 50 base reads. Longer
reads cost more because more cycles use up more reagents, and the
reagents are expensive.

This step produces an enormous batch (100 million or so) of
sequences. You can stop there if you want - if you do, then you've
done what is called "single-end sequencing" because you only got the
sequence for one end of the millions of strings you dumped into the
sequencer.  However, you can also keep going and produce paired end
reads. If you do this, then after the first pass run is finished, the
machine switches over to reading the other end of the glued-on
strings. It does this by first copying over the strings in such a way that
guarantees that the part it sequenced previously gets stuck to the plate.
And when it sequences the other end of each string, it produces the
reverse complement of the strand it sequenced originally.

Going back to the diagram, here's what you
get when you do a paired-end read:

Ex)

```
               --->
Forward strand: A G T C T C C G G
Reverse strand: T C A G A G G C C 
                              <---
first pass read: A G T C (this is read 1)
second pass read: C C G G (this is read 2)
```

In this example, the reads A G T C and C C G G are called "paired end"
reads because they came from the same original string. However, they
are in opposite orientation because one came directly from the forward
strand, reading from left to right, and the other one came directly
from the reverse strand, reading from right to left.

## Alignment step - making BAM files

Once you're finished getting the sequences of the strings, the next step is 
to align them onto some reference sequence, usually a genome assembly.

A genome assembly is another set of double-strand strings, but it usually 
has ten to twenty extremely large strings. For example, the Arabidopsis 
genome assembly has seven strings which consists of many millions of letters each.

When we "align" a read onto a genome, what we are doing is finding a
substring within the much larger string (called a chromosome,
typically) that is identical to our read's sequence. Programs like
bowtie and tophat perform this alignment. 

The alignments may not be perfect however - they can
include gaps or mismatches. Also, the alignments have
a strand - they indicate whether the read aligned onto (matched) the
forward or the reverse strand of the reference sequence. Morever, in
general, the two members of a pair will align to opposite strands of
the reference. However, there may be multiple locations where a read
may align. And sometimes, one or both members of a pair may fail to
align.

Programs like tophat that generate read alignments and BAM output data
handle these complications using a "bit flag" in one field of the SAM
record. See the SAM format for details. However, there's a handy tool
you can use to explore what the bit flags mean:

* Tool to explain bit flags: http://picard.sourceforge.net/explain-flags.html

## How pair members are named

How Illumina instruments have used a variety of methods over the years 
for indicating whether reads are paired-end or not. 

The current incarnation of the software
(called Cassava 1.8) reports paired end reads using the FASTQ format
and it indicates members of a pair using a somewhat inconvenient
naming convention. For details, see:

http://en.wikipedia.org/wiki/FASTQ_format#Illumina_sequence_identifiers

In a nutshell, it reports the name and pair affiliation for sequences
using two strings in the "name" field of the FASTQ record.

Previous incarnations of the instrument would append \1 or \2 on the
ends of names to indicate whether two reads came from the same string
or not.

Thus, when reading a BAM file and processing alignments for display in
IGB, you can't count on matching names only to identify alignments for
ends of a pair. Instead, what you have to do is look at the "bit flag"
to determine whether a given read is the first in a pair, the second
in a pair, whether or not its mate aligned, and so on.

Luckily, you can get all of this information when you parse the reads
using the Picard library. The class you want to pay particular 
attentoin to is called SAMRecord; it has methods like 

```
getMateAlignmentStart()
getMateAlignmentEnd()
```

and so forth that enable you to quickly find out whether a read has
a mate and where it is. Thus, when you read BAM data into IGB, you
should capture this information. I would recommend storing the bit
flag (since it's compact) and also storing the start position of mated
reads so that you can easily retrieve them from a BAM file necessary. 
There are many possible optizizations to try that can make the process
of accessing and rendering short reads faster. However, to start,
probably you'll just want to get something displayed so that we
can experiment.
