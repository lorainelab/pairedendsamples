#!/bin/bash

files=`ls *Overlap*.sam`
for file in $files; do
    prefix=${file%.sam}
    echo $prefix
    samtools view -bS $file > tmp.bam
    samtools sort tmp.bam $prefix
    rm tmp.bam
    samtools index $prefix.bam
done
        
